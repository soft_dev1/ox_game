/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.ox_game;

import java.util.Scanner;

/**
 *
 * @author Admin
 */


public class OX {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int row = 0, column = 0;
        System.out.println("Welcome to OX GAME");
         //create table
        char board[][] = new char[4][4];
        //set player
        char player1 = 'X';
        char player2 = 'O';
        
        //set - in table
         for(int i = 1; i<4; i++){
            for(int j = 1; j<4; j++){
                board[i][j] = '-';
            }
        }
         
        boolean user = true;
	boolean gameEnd = false;
        //loop while for check game end.
        while (!gameEnd) { 
            showBoard(board);
            char symbol = ' ';
                if (user) {
                    symbol = 'X';
		} else {
                    symbol = 'O';
		}

            if (user) {
		System.out.println(player1 + " turn");
            } else {
		System.out.println(player2 + " turn");
            }

        //loop while for input 
        while(true){
            
            System.out.println("Please input Row Col: ");
            row = kb.nextInt();
            column = kb.nextInt();
       
            // if-else for checking when the user enters the wrong value
            if (row < 0 || row > 3 || column < 0 || column > 3) {
				System.out.println("Row and Column must be number 1-3 please try again.");
			} else if (board[row][column] != '-') {
				System.out.println("Row and Column cann't choose again.");
			} else {
				break;
			}
        }
           //Replace row and column with X and O
            board[row][column] = symbol;
            
        //Check the position of X
	if (checkWinner(board) == 'X') { 
            showBoard(board);
            System.out.println("Player " + player1 + " Win....");
            System.out.println("Bye bye ....");
            gameEnd = true;
	//Check the position of O
	} else if (checkWinner(board) == 'O') { // check position is O
            showBoard(board);
            System.out.println("Player " + player2 + " Win....");
            System.out.println("Bye bye ....");
            gameEnd = true;
	//Check the position when there is a draw
	} else {
            if (checkDraw(board)) { 
		showBoard(board);
		System.out.println("The game ended in a draw.");
		System.out.println("Bye bye ....");
		gameEnd = true;
	} else {
            user = !user; // true = false
	}
    }
}

}
	//For printing X or O on the screen
        public static void showBoard(char board[][]){
            System.out.println(" 1 2 3");
            for(int i = 1; i<4; i++){
                System.out.print(i);
                for(int j = 1; j<4; j++){
                    System.out.print(board[i][j]+ " ");
                }
                System.out.println();
            }
        }
        
	//To check for winners
	public static char checkWinner(char board[][]) {
            // In case win for row
            for (int i = 1; i < 4; i++) {
		if (board[i][1] == board[i][2] && board[i][2] == board[i][3] && board[i][1] != '-') {
                    return board[i][1];
		}
            }

            // In case wn for column
            for (int j = 1; j < 4; j++) {
		if (board[1][j] == board[2][j] && board[2][j] == board[3][j] && board[1][j] != '-') {
                    return board[1][j];
		}
            }

            // In case win for diagonal
            if (board[1][1] == board[2][2] && board[2][2] == board[3][3] && board[1][1] != '-') {
		return board[1][1];
            }
            if (board[3][1] == board[2][2] && board[2][2] == board[1][3] && board[3][1] != '-') {
		return board[3][1];
            }

            // return to player X to O and O to X
		return '-';

	

	}
	public static boolean checkDraw(char board[][]) {
            // return to player X and O
            for (int i = 1; i < 4; i++) {
		for (int j = 1; j < 4; j++) {
                    if (board[i][j] == '-') {
			return false;
                    }   
		}
            }
	// check board is draw
            return true;
    }
}